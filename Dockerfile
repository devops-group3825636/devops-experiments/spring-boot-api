FROM maven:3.8.3-openjdk-17
WORKDIR /app
COPY pom.xml .
COPY src ./src
RUN mvn clean package -DskipTests
EXPOSE 8081
ENTRYPOINT ["java","-jar","target/app.jar"]
